public class Main {

    public static void main(String[] args) {
        Triangle ravno = new Triangle(4,4,4);
        Triangle pryamo = new Triangle(3, 5, 8);
        Triangle simple = new Triangle(4.6, 5.2, 6);

        System.out.println(ravno.getAreaOfTriangle());
        System.out.println(pryamo.getAreaOfTriangle());
        System.out.println(simple.getAreaOfTriangle());
    }
}
