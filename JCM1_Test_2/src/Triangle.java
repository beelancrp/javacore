/**
 * Created by bilan on 07.09.2016.
 */
public class Triangle {

    private double a,b,c;

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getAreaOfTriangle(){
        if (a + b == c){
            return 0.5 * a * b;
        }else if(a == b && a == c){
            return (Math.sqrt(3) * (a * a))/4;
        }else {
            double p = (a + b + c)/2;
            return Math.sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
}
