/**
 * Created by bilan on 06.09.2016.
 */
public class Cat {

    private String eyeColor, bodyColor, name;
    private int numberOfLags;
    private double age;

    public Cat(){

    }

    public Cat(String eyeColor, String bodyColor, String name, double age, int numberOfLags) {
        this.eyeColor = eyeColor;
        this.bodyColor = bodyColor;
        this.name = name;
        this.age = age;
        this.numberOfLags = numberOfLags;
    }

    public void mew(){
        System.out.println("Mew-Mew-Mew!");
    }

    public void eat(){
        System.out.println("Om-nom-nom!");
    }

    public boolean run(String where){
        if (where != null){
            if (numberOfLags == 4){
                System.out.println(name + ": I'm running to " + where);
                return true;
            }else {
                System.out.println(name + ": I can not run, i'm invalid!");
                return false;
            }
        }else {
            return false;
        }
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getBodyColor() {
        return bodyColor;
    }

    public void setBodyColor(String bodyColor) {
        this.bodyColor = bodyColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfLags() {
        return numberOfLags;
    }

    public void setNumberOfLags(int numberOfLags) {
        this.numberOfLags = numberOfLags;
    }

    public double getAge() {
        return age;
    }

    public void setAge(float age) {
        this.age = age;
    }
}
