/**
 * Created by bilan on 06.09.2016.
 */
public class Main {

    public static void main(String[] args) {
        Cat cat1 = new Cat("red", "green", "Maiami", 1.5, 4);
        Cat cat2 = new Cat("blue", "brown", "Cotan", 1.5, 4);
        Cat cat3 = new Cat("yellow", "gray", "Murzik", 1.5, 4);

        cat1.eat();
        cat2.mew();
        cat3.run("magaz");
    }
}
