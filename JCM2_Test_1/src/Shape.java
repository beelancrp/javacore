/**
 * Created by bilan on 07.09.2016.
 */
public abstract class Shape {

    abstract double perimetr();
    abstract double area();
}
