
/**
 * Created by bilan on 07.09.2016.
 */
public class Rectangle extends Shape {

    Point sidesWidth;

    public Rectangle(Point sidesWidth) {
        this.sidesWidth = sidesWidth;
    }

    @Override
    double perimetr() {
        return 2 * (sidesWidth.getA() + sidesWidth.getB());
    }

    @Override
    double area() {
        return (sidesWidth.getA() * sidesWidth.getB());
    }
}
