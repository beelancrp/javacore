/**
 * Created by bilan on 07.09.2016.
 */
public class Vector implements IVector {

    Point pointA, pointB, vectorCoordinats;

    public Vector(){

    }

    public Vector(Point A, Point B) {
        this.pointA = A;
        this.pointB = B;
        this.vectorCoordinats = new Point(
                pointB.getX() - pointA.getX(),
                pointB.getY() - pointA.getY(),
                pointB.getZ() - pointA.getZ()
        );
    }



    public Point getVectorCoordinats() {
        return vectorCoordinats;
    }

    public void setVectorCoordinats(Point vectorCoordinats) {
        this.vectorCoordinats = vectorCoordinats;
    }

    @Override
    public Vector sum(Vector vector2) {
        Vector temp = new Vector();
        temp.setVectorCoordinats(new Point(
                this.getVectorCoordinats().getX() + vector2.getVectorCoordinats().getX(),
                this.getVectorCoordinats().getY() + vector2.getVectorCoordinats().getY(),
                this.getVectorCoordinats().getZ() + vector2.getVectorCoordinats().getZ()
        ));
        return temp;
    }

    @Override
    public int scalarMultiply(Vector vector2) {
        return (this.getVectorCoordinats().getX() * vector2.getVectorCoordinats().getX())
                +
                (this.getVectorCoordinats().getY() * vector2.getVectorCoordinats().getY())
                +
                (this.getVectorCoordinats().getZ() * vector2.getVectorCoordinats().getZ());
    }

    @Override
    public Vector vectorMultiply(Vector vector2) {
        Vector temp = new Vector();
        temp.setVectorCoordinats(new Point(
                (this.getVectorCoordinats().getY() * vector2.getVectorCoordinats().getZ()) - (this.getVectorCoordinats().getZ() * vector2.getVectorCoordinats().getY()),
                -1 * ((this.getVectorCoordinats().getZ() * vector2.getVectorCoordinats().getX())-(this.getVectorCoordinats().getX() * vector2.getVectorCoordinats().getZ())),
                (this.getVectorCoordinats().getX() * vector2.getVectorCoordinats().getY()) - (this.getVectorCoordinats().getY() * vector2.getVectorCoordinats().getX())
        ));
        return temp;
    }
}
