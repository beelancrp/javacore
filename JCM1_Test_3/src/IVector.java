/**
 * Created by bilan on 07.09.2016.
 */
public interface IVector {

    Vector sum(Vector vector2);
    int scalarMultiply(Vector vector2);
    Vector vectorMultiply(Vector vector2);
}
