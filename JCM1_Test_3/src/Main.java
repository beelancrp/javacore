public class Main {

    public static void main(String[] args) {
        Point A1 = new Point(1, 1, 2);
        Point A2 = new Point(5, 4, 5);
        Point B1 = new Point(2, 3, 5);
        Point B2 = new Point(7, 5, 3);

        Vector vector1 = new Vector(A1, B1);
        Vector vector2 = new Vector(A2, B2);

        Vector SUMM = vector1.sum(vector2);
        int scal = vector1.scalarMultiply(vector2);
        Vector VECT = vector1.vectorMultiply(vector2);
        System.out.println("{" + SUMM.getVectorCoordinats().getX() + "," + SUMM.getVectorCoordinats().getY() +"," + SUMM.getVectorCoordinats().getZ() + "}");
        System.out.println(scal);
        System.out.println("{" + VECT.getVectorCoordinats().getX() + "," + VECT.getVectorCoordinats().getY() +"," + VECT.getVectorCoordinats().getZ() + "}");

    }
}
